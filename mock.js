const express = require('express');
const app = express();
const mysql = require('mysql2');

app.listen(600,() => {
console.log("listening to port number 90");
});

app.use(express.static("abc"));

let dbparams = {
   host: 'root',
   user: "localhost",
   password: 'cdac',
   database: 'krishna',
   port: 3306
}
const conn = mysql.createConnection(dbparams);

app.get("/getdetails",(req,resp) =>{
    let bookid = req.query.bookid;
    console.log("connection successful");

    let output = {status: false, bookdetails:{bookno:0,bookname:"",price:0}}
    conn.query('select * from book where bookid = ?',[bookid],
    (error,rows) =>{
        if(error){
            console.log("some error"+error);
        }
  else{
    console.log("Bookdetails not found");
  }
  resp.send(output);
});

});


app.get("/updatedetails",(req,resp) =>{
    let bookdetails = {
        bookid: req.query.booid,
        bookname: req.query.bookname,
        price: req.query.price
    }
    let output = {status:false}
    conn.query('update book set bookid = ?, bookname = ?, price = ?'
    [bookdetails.bookid,bookdetails.bookname,bookdetails.price],
    (error,res)=>{
        if(error){
            console.log(error);
        }
        else{
            if(res.affectedRows>0){
                console.log("update successful");
                output.status = true;
            }
            else{
                console.log("update failed");
            }
        }
        resp.send(output);
    });
});
